FROM docker.io/maven:3 as builder
WORKDIR /tmp

# proactive-fraud-detection-case - ui
COPY rhpam-case-mgmt-showcase/rhpam-case-mgmt-showcase.war /tmp/rhpam-case-mgmt-showcase-1.0.0-SNAPSHOT.war
RUN mvn install:install-file \
    -Dfile=/tmp/rhpam-case-mgmt-showcase-1.0.0-SNAPSHOT.war \
    -DgroupId=com.rhpam.case \
    -DartifactId=showcase \
    -Dversion=1.0.0-SNAPSHOT \
    -Dpackaging=war \
    -DlocalRepositoryPath=/tmp/bucketrepo

## proactive-fraud-detection-fuse
RUN git clone https://gitlab.com/2020-summit-labs/proactive-fraud-detection/proactive-fraud-detection-fuse.git && \
    cd /tmp/proactive-fraud-detection-fuse && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## proactive-fraud-detection-kafka
    cd /tmp/ && \
    git clone https://gitlab.com/2020-summit-labs/proactive-fraud-detection/proactive-fraud-detection-kafka.git && \
    cd /tmp/proactive-fraud-detection-kafka && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -P native -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -P native -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## proactive-fraud-detection-case
    cd /tmp/ && \
    git clone https://gitlab.com/2020-summit-labs/proactive-fraud-detection/proactive-fraud-detection-case.git && \
    cd /tmp/proactive-fraud-detection-case && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## proactive-fraud-detection-dmn
    cd /tmp/ && \
    git clone https://gitlab.com/2020-summit-labs/proactive-fraud-detection/proactive-fraud-detection-dmn.git && \
    cd /tmp/proactive-fraud-detection-dmn && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    ## proactive-fraud-detection-kclient
    cd /tmp/ && \
    git clone https://gitlab.com/2020-summit-labs/proactive-fraud-detection/proactive-fraud-detection-kclient.git && \
    cd /tmp/proactive-fraud-detection-kclient && \
    mvn clean install -DskipTests -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn de.qaware.maven:go-offline-maven-plugin:resolve-dependencies -DdownloadSources -DdownloadJavadoc -B -Dmaven.repo.local=/tmp/bucketrepo && \
    mvn dependency:go-offline -B -Dmaven.repo.local=/tmp/bucketrepo && \
    cd /tmp/bucketrepo && \
    find -iname "*.repositories" -exec rm -f {} \;

# Remove built workloads
RUN rm -rf /tmp/bucketrepo/com/redhat/ukconsulting/summit2020/proactive-fraud-detection-fuse \
    rm -rf /tmp/bucketrepo/com/redhat/summit/fraud-detection-pam-kafka-producer \
    rm -rf /tmp/bucketrepo/com/redhat/summit/fraud-detection-pam-kafka-aggregator \
    rm -rf /tmp/bucketrepo/com/demo/proactive-fraud-detection-case \
    rm -rf /tmp/bucketrepo/com/redhat/demo/proactive-fraud-detection-dmn

FROM gcr.io/jenkinsxio/bucketrepo:0.1.19
COPY --from=builder /tmp/bucketrepo /tmp/bucketrepo
COPY fix-permissions /usr/bin/fix-permissions
RUN chmod 775 /usr/bin/fix-permissions && \
    /usr/bin/fix-permissions /tmp/bucketrepo