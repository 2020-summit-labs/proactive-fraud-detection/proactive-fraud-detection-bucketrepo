#!/usr/bin/env bash

curl -X POST -H "Content-Type: application/json" -H "Authorization: Bearer ${QUAY_TOKEN}" "https://quay.io/api/v1/repository/redhat-consulting-uk/proactive-fraud-detection-bucketrepo/trigger/${QUAY_TRIGGER_ID}/start" -d '{"commit_sha":"'"${CI_COMMIT_SHA}"'"}'